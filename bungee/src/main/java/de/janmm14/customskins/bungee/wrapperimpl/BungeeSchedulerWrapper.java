package de.janmm14.customskins.bungee.wrapperimpl;

import javax.annotation.Nullable;

import net.md_5.bungee.api.scheduler.TaskScheduler;

import de.janmm14.customskins.bungee.CustomSkinsBungee;
import de.janmm14.customskins.core.wrapper.SchedulerWrapper;

import lombok.NonNull;

public class BungeeSchedulerWrapper implements SchedulerWrapper {

	@NonNull
	private final CustomSkinsBungee plugin;
	private final TaskScheduler scheduler;

	public BungeeSchedulerWrapper(@NonNull CustomSkinsBungee plugin) {
		this.plugin = plugin;
		scheduler = plugin.getProxy().getScheduler();
	}

	@Override
	public void asyncNow(@NonNull Runnable task) {
		scheduler.runAsync(plugin, task);
	}

	@Nullable
	@Override
	public Object getWrapped() {
		return scheduler;
	}
}
