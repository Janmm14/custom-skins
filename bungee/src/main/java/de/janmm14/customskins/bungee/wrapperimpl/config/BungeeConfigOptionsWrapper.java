package de.janmm14.customskins.bungee.wrapperimpl.config;

import de.janmm14.customskins.core.wrapper.config.ConfigOptionsWrapper;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

public class BungeeConfigOptionsWrapper implements ConfigOptionsWrapper {

	@NonNull
	@Override
	public ConfigOptionsWrapper copyDefaults(boolean value) {
		return this;
	}

	@Override
	@Nullable
	public Object getWrapped() {
		return this;
	}
}
