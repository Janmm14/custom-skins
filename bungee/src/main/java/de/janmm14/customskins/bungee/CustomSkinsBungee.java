package de.janmm14.customskins.bungee;

import java.util.Set;
import java.util.UUID;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.protocol.ProtocolConstants;

import de.janmm14.customskins.bungee.wrapperimpl.BungeeSchedulerWrapper;
import de.janmm14.customskins.bungee.wrapperimpl.config.BungeeConfigWrapper;
import de.janmm14.customskins.core.data.Data;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;
import de.janmm14.customskins.core.wrapper.SchedulerWrapper;
import de.janmm14.customskins.core.wrapper.SkinHandler;
import de.janmm14.customskins.core.wrapper.config.ConfigWrapper;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NonNull;

@Getter
public class CustomSkinsBungee extends Plugin implements PluginWrapper {

	@Getter(onMethod = @__({@Deprecated}))
	private static CustomSkinsBungee plugin;

	private Data data;
	private SkinHandler skinHandler;
	private ConfigWrapper configWrapper;
	private SchedulerWrapper schedulerWrapper;

	@Override
	public void onEnable() {
		getLogger().severe("This plugin is not yet implemented for bungeecord and therefore will not have any functionality.");
		if (true) { //abort here for now as plugin is not functioning yet
			return;
		}
		checkCompatibility();

		configWrapper = new BungeeConfigWrapper(this);
		schedulerWrapper = new BungeeSchedulerWrapper(this);
		data = new Data(this);
		skinHandler = new BungeePacketsSkinHandler(this);
	}

	private void checkCompatibility() {
		if (ProxyServer.getInstance().getProtocolVersion() < ProtocolConstants.MINECRAFT_1_8) {
			getLogger().severe("Error! This plugin only supports 1.8!");
			onDisable();
			return;
		}
		for (ProxiedPlayer plr : getProxy().getPlayers()) {
			if (plr.getPendingConnection().getVersion() < ProtocolConstants.MINECRAFT_1_8) {
				getLogger().severe("Error! This plugin only supports 1.8!");
				return;
			}
		}
	}

	@Override
	public void onDisable() {
		getLogger().severe("This plugin is not yet implemented for bungeecord and therefore did not had any functionality.");
	}

	@Override
	public CommandSenderWrapper getPlayer(@NonNull UUID uuid) { //TODO implement
		return null;
	}

	@Override
	public CommandSenderWrapper getPlayer(@NonNull String name) { //TODO implement
		return null;
	}

	@NonNull
	@Override
	public Set<CommandSenderWrapper> getOnlinePlayers() { //TODO implement
		return null;
	}

	@Override
	@Nullable
	public CustomSkinsBungee getWrapped() { //TODO implement
		return this;
	}
}
