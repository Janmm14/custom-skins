package de.janmm14.customskins.bungee.wrapperimpl.config;

import java.io.File;
import java.io.IOException;

import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import de.janmm14.customskins.bungee.CustomSkinsBungee;
import de.janmm14.customskins.core.wrapper.config.ConfigOptionsWrapper;
import de.janmm14.customskins.core.wrapper.config.ConfigWrapper;

import lombok.NonNull;
import lombok.SneakyThrows;

public class BungeeConfigWrapper extends BungeeConfigurationSectionWrapper implements ConfigWrapper {

	private final ConfigurationProvider provider;
	private final File file;
	private final BungeeConfigOptionsWrapper configOptionsWrapper;

	public BungeeConfigWrapper(CustomSkinsBungee plugin) {
		provider = ConfigurationProvider.getProvider(YamlConfiguration.class);
		file = new File(plugin.getDataFolder(), "config.yml");
		reloadConfig();
		configOptionsWrapper = new BungeeConfigOptionsWrapper();
	}

	@Override
	@SneakyThrows(IOException.class)
	public void reloadConfig() {
		cfg = provider.load(file);
	}

	@Override
	@SneakyThrows(IOException.class)
	public void saveConfig() {
		provider.save(cfg, file);
	}

	@NonNull
	@Override
	public ConfigOptionsWrapper options() {
		return configOptionsWrapper;
	}

}
