package de.janmm14.customskins.bungee.wrapperimpl;

import java.util.UUID;
import javax.annotation.Nullable;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;

import lombok.NonNull;

public class BungeeCommandSenderWrapper implements CommandSenderWrapper {

	@NonNull
	private final CommandSender sender;

	public BungeeCommandSenderWrapper(@NonNull CommandSender sender) {
		this.sender = sender;
	}

	@NonNull
	@Override
	public String getName() {
		return sender.getName();
	}

	@NonNull
	@Override
	public UUID getUuid() {
		if (isPlayer()) {
			return ((ProxiedPlayer) sender).getUniqueId();
		}
		return CONSOLE_UUID;
	}

	@Override
	public void sendMessage(@NonNull String legacyChat) {
		sender.sendMessage(legacyChat);
	}

	@Override
	public void sendMessage(@NonNull BaseComponent component) {
		sender.sendMessage(component);
	}

	@Override
	public void sendMessage(@NonNull BaseComponent... components) {
		sender.sendMessage(components);
	}

	@Override
	public boolean hasPermission(@NonNull String permission) {
		return sender.hasPermission(permission);
	}

	@NonNull
	@Override
	public Type getType() {
		return sender instanceof ProxiedPlayer ? Type.PLAYER : Type.CONSOLE;
	}

	@Nullable
	@Override
	public Object getWrapped() {
		return sender;
	}
}
