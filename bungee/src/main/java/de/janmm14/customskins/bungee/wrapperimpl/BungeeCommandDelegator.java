package de.janmm14.customskins.bungee.wrapperimpl;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import de.janmm14.customskins.core.commands.CommandHandler;

public class BungeeCommandDelegator extends Command {

	private final CommandHandler commandHandler;

	public BungeeCommandDelegator(CommandHandler commandHandler) {
		super("customskins", null, "cs");
		this.commandHandler = commandHandler;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		commandHandler.onCommand(new BungeeCommandSenderWrapper(sender), "customskins", "customskins", args);
	}
}
