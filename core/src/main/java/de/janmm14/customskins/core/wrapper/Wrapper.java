package de.janmm14.customskins.core.wrapper;

import javax.annotation.Nullable;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface Wrapper {

	@Nullable
	Object getWrapped();
}
