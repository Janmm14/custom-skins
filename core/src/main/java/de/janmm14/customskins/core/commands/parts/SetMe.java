package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.PlayerOnlyCmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import lombok.NonNull;

public class SetMe extends PlayerOnlyCmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins setme <id/name>").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins setme "))
		.append(" - ").color(ChatColor.GRAY)
		.append("sets your skin to the skin of the given playername or the downloaded skin").color(ChatColor.YELLOW).create();

	public SetMe(PluginWrapper plugin) {
		super(plugin, "customskins.setme", "setme");
	}

	@Override
	public void onPlayerCommand(@NonNull final CommandSenderWrapper p, @NonNull final String[] args) {
		getPlugin().getSchedulerWrapper().asyncNow(() -> {
			String skinId = args[0].toLowerCase();
			if (getPlugin().getData().getCachedSkin(skinId) == null) {
				p.sendMessage("§cSkin " + skinId + "not found!");
			}
			getPlugin().getData().setUsedSkin(p.getUuid(), skinId);
			getPlugin().getSkinHandler().updatePlayerSkin(p.getUuid());
			p.sendMessage("§aSkin updated!");
		});
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		// TODO implement
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
