package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.data.Proxy;
import de.janmm14.customskins.core.data.Skin;
import de.janmm14.customskins.core.util.Network;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class Get extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins get <playername>").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins get "))
		.append(" - ").color(ChatColor.GRAY)
		.append("prepares the skin of the given player to be set").color(ChatColor.YELLOW).create();

	public Get(PluginWrapper plugin) {
		super(plugin, "customskins.get", "get", "g");
	}

	@Override
	public void onCommand(@NonNull final CommandSenderWrapper cs, @NonNull final String[] args) {
		if (args.length < 1) {
			sendUsage(cs);
			return;
		}
		// TODO better chat messages
		getPlugin().getSchedulerWrapper().asyncNow(() -> {
			cs.sendMessage("Retrieving skin's data...");
			Proxy proxy = null;
			String skinId = args[0];
			String name = skinId.toLowerCase();
			UUID uuid = Network.getUuidsOfNames(name).values().iterator().next();

			for (Proxy p : getPlugin().getData().getProxies()) {
				if ((p.getLastUsedMillis(uuid) + (1000 * 75)) < System.currentTimeMillis()) {
					proxy = p;
					break;
				}
			}

			if (proxy == null) {
				cs.sendMessage("§cCurrently is no proxy available to handle your request.");
				return;
			}

			Skin skin = Network.getSkin(uuid, proxy, name);
			if (skin == null) {
				cs.sendMessage("§cAn error occurred while trying to recieve the skin's data.");
				return;
			}
			proxy.setLastUsedMillis(uuid, System.currentTimeMillis());
			getPlugin().getData().setCachedSkin(skin, "from_user: " + skinId);
			cs.sendMessage("Skin data loaded. Skin " + name + " is ready for usage with /customskins set");
		});
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper cs, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
