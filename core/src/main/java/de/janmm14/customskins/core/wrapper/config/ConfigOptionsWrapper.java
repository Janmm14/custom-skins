package de.janmm14.customskins.core.wrapper.config;

import de.janmm14.customskins.core.wrapper.Wrapper;

import lombok.NonNull;

public interface ConfigOptionsWrapper extends Wrapper {

	@NonNull
	ConfigOptionsWrapper copyDefaults(boolean value);
}
