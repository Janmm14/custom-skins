package de.janmm14.customskins.core.wrapper.config;

import de.janmm14.customskins.core.wrapper.Wrapper;

import lombok.NonNull;

public interface ConfigWrapper extends ConfigurationSectionWrapper, Wrapper {

	void reloadConfig();

	void saveConfig();

	@NonNull
	ConfigOptionsWrapper options();
}
