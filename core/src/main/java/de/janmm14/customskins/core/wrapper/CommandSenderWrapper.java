package de.janmm14.customskins.core.wrapper;

import java.util.UUID;

import net.md_5.bungee.api.chat.BaseComponent;

import lombok.NonNull;

public interface CommandSenderWrapper extends Wrapper {

	UUID CONSOLE_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

	@NonNull
	String getName();

	@NonNull
	UUID getUuid();

	void sendMessage(@NonNull String legacyChat);

	void sendMessage(@NonNull BaseComponent component);

	void sendMessage(@NonNull BaseComponent... components);

	boolean hasPermission(@NonNull String permission);

	@NonNull
	CommandSenderWrapper.Type getType();

	default boolean isPlayer() {
		return getType() == Type.PLAYER;
	}

	enum Type {
		PLAYER, CONSOLE
	}
}
