package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.PlayerOnlyCmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class ResetMe extends PlayerOnlyCmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins resetme").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins resetme"))
		.append(" - ").color(ChatColor.GRAY)
		.append("resets your skin to the default minecraft.net one").color(ChatColor.YELLOW).create();

	public ResetMe(PluginWrapper plugin) {
		super(plugin, "customskins.resetme", "resetme");
	}

	@Override
	public void onPlayerCommand(@NonNull CommandSenderWrapper p, @NonNull String[] restArgs) {
		getPlugin().getData().resetSkin(p.getUuid());
		getPlugin().getSkinHandler().updatePlayerSkin(p.getUuid());
		p.sendMessage("§aYour skin has been reset. You maybe have to rejoin to see the changes yourself.");
	}

	@NonNull
	@Override
	public java.util.List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
