package de.janmm14.customskins.core.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.annotation.Nullable;

import de.janmm14.customskins.core.data.Proxy;
import de.janmm14.customskins.core.data.Skin;

import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import lombok.NonNull;

public final class Network {

	@NonNull
	public static final JSONParser JSON_PARSER = new JSONParser();

	private static final String NAME_FETCHER_URL = "https://api.mojang.com/profiles/minecraft";
	private static final int MAX_NAMES_PER_REQUEST = 100;

	private static final double MAX_NAMES_PER_REQUEST_D = MAX_NAMES_PER_REQUEST;
	public static boolean applyRateLimit = true;

	private Network() {
		throw new UnsupportedOperationException();
	}

	private static RequestConfig.Builder getOrDefault(HttpRequestBase requestBase) {
		return RequestConfig.copy(requestBase.getConfig() == null ? RequestConfig.DEFAULT : requestBase.getConfig());
	}

	public static boolean downloadSkin(@NonNull String url, @NonNull File file) {
		try {
			if (!url.toLowerCase().startsWith("http")) {
				url = "http://" + url;
			}
			CloseableHttpClient client = HttpClients.createDefault();

			HttpGet httpGet = new HttpGet(url);
			httpGet.setConfig(getOrDefault(httpGet).setRedirectsEnabled(true).setRelativeRedirectsAllowed(true).build());

			CloseableHttpResponse response = client.execute(httpGet);

			HttpEntity entity = response.getEntity();
			if (entity == null) {
				return false;
			}

			InputStream input = entity.getContent();
			if (file.exists() && !file.delete()) {
				System.err.println("[CustomSkins] Could not delete old file " + file.getAbsolutePath());
				return false;
			}
			if (!file.createNewFile()) {
				System.err.println("[CustomSkins] Could not create file " + file.getAbsolutePath());
				return false;
			}

			FileOutputStream fileOut = new FileOutputStream(file);
			IOUtils.copy(input, fileOut);
			fileOut.flush();
			fileOut.close();
			input.close();
			response.close();
			client.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Call only asychroniously
	 *
	 * @param uuid uuid to get the skin from
	 * @param proxy the proxy to use or null if no proxy should be used
	 * @param skinName the name the skin should have at the end
	 * @return the skin data
	 */
	@Nullable
	public static Skin getSkin(@NonNull UUID uuid, @Nullable Proxy proxy, @NonNull String skinName) {
		Skin skin = null;
		if (proxy != null && proxy.isNoProxy()) {
			proxy = null;
		}
		try {
			CloseableHttpClient client;
			// add proxy credentials if needed
			if (proxy != null && proxy.getUsername() != null && !proxy.getUsername().trim().isEmpty()) {
				CredentialsProvider proxyAuth = new BasicCredentialsProvider();
				proxyAuth.setCredentials(new AuthScope(proxy.getHost(), proxy.getPort()), new UsernamePasswordCredentials(proxy.getUsername(), proxy.getPassword()));
				client = HttpClients.custom().setDefaultCredentialsProvider(proxyAuth).build();
			} else {
				client = HttpClients.createDefault();
			}
			// build the request
			HttpGet post = new HttpGet("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.toString().replaceAll("-", "") + "?unsigned=false");
			if (proxy != null) {
				RequestConfig.Builder builder = getOrDefault(post);
				post.setConfig(builder.setProxy(new HttpHost(proxy.getHost(), proxy.getPort())).build());
			}
			post.setHeader("Content-Type", "application/json");

			// execute and error handling
			CloseableHttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			if (entity == null) {
				StatusLine statusLine = response.getStatusLine();
				System.err.println("[CustomSkins] Could not recieve skin data for " + uuid + " through proxy " + proxy + ": Status code " + statusLine.getStatusCode() + ": " + statusLine.getReasonPhrase());
				return null;
			}
			String res = EntityUtils.toString(entity);

			// parse
			JSONObject json = (JSONObject) JSON_PARSER.parse(res);
			JSONArray obj = (JSONArray) json.get("properties");
			JSONObject properties = (JSONObject) obj.get(0);

			String value = (String) properties.get("value");
			String signature = (String) properties.get("signature");

			skin = new Skin(skinName, value, signature);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return skin;
	}

	/**
	 * Call only asynchroniously!
	 *
	 * @param names the names to get the uuids from
	 * @return player names mapped to their uuids
	 */
	@NonNull
	public static Map<String, UUID> getUuidsOfNames(@NonNull Set<String> names) {
		// no duplicates possible because of provided collection is a set
		return getUuidsOfNames0(new ArrayList<>(names));
	}

	/**
	 * Call only asynchroniously!
	 *
	 * @param names the names to get the uuids from
	 * @return player names mapped to their uuids
	 */
	@NonNull
	public static Map<String, UUID> getUuidsOfNames(@NonNull String... names) {
		return getUuidsOfNames(Arrays.asList(names));
	}

	/**
	 * Call only asynchroniously!
	 *
	 * @param names the names to get the uuids from
	 * @return player names mapped to their uuids
	 */
	@NonNull
	public static Map<String, UUID> getUuidsOfNames(@NonNull List<String> names) {
		// ensure no duplicates
		return getUuidsOfNames0(new ArrayList<>(new HashSet<>(names)));
	}

	/**
	 * Call only asynchroniously!
	 *
	 * @param names the names to get the uuids from
	 * @return player names mapped to their uuids
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	private static Map<String, UUID> getUuidsOfNames0(@NonNull List<String> names) {
		Map<String, UUID> results = Maps.newHashMapWithExpectedSize(names.size());
		int requests = (int) Math.ceil(names.size() / MAX_NAMES_PER_REQUEST_D);
		for (int i = 0; i < requests; i++) {
			if (applyRateLimit && i != 0) {
				try {
					Thread.sleep(100L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				HttpURLConnection connection = createMinecraftConnection(NAME_FETCHER_URL, RequestMethod.POST);
				String body = JSONArray.toJSONString(names.subList(i * MAX_NAMES_PER_REQUEST, Math.min((i + 1) * MAX_NAMES_PER_REQUEST, names.size())));
				writePostData(connection, body.getBytes()).close();
				Iterable<Map<String, String>> array = (Iterable<Map<String, String>>) JSON_PARSER.parse(new InputStreamReader(connection.getInputStream()));
				for (Map<String, String> profile : array) {
					String dashFreeUuid = profile.get("id");
					String name = profile.get("name");
					UUID uuid = UUID.fromString(Util.insertDashesToUuidString(dashFreeUuid));
					results.put(name, uuid);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return results;
	}

	@NonNull
	private static HttpURLConnection createMinecraftConnection(@NonNull String url, @NonNull RequestMethod requestMethod) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		connection.setRequestMethod(requestMethod.name());
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("User-Agent", "minecraft");
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		return connection;
	}

	@NonNull
	private static OutputStream writePostData(@NonNull URLConnection connection, @NonNull byte[] data) throws IOException {
		OutputStream stream = connection.getOutputStream();
		stream.write(data);
		stream.flush();
		return stream;
	}

	private enum RequestMethod {
		GET, POST
	}

}
