package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class Reload extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins reload").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins reload"))
		.append(" - ").color(ChatColor.GRAY)
		.append("reloads the configuration").color(ChatColor.YELLOW)
		.create();

	public Reload(PluginWrapper plugin) {
		super(plugin, "customskins.reload", "reload");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		getPlugin().getData().reloadConfig();
		sender.sendMessage("§aConfiguration reloaded!");
	}

	@Override
	@NonNull
	public List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
