package de.janmm14.customskins.core.wrapper;

import java.util.logging.Logger;

import lombok.NonNull;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface LoggerProvider {

	@NonNull
	Logger getLogger();
}
