package de.janmm14.customskins.core.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import org.jetbrains.annotations.Contract;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@ToString
@EqualsAndHashCode(exclude = {"id", "lastUsedMillis"})
public class Proxy {

	public static final Proxy NO_PROXY = new Proxy("", "", Integer.MIN_VALUE + 8157, null, null);
	@NonNull
	private final String id;
	@NonNull
	private final String host;
	private final int port;
	private final String username;
	private final String password;
	private final transient Map<UUID, Long> lastUsedMillis = new HashMap<>();

	@Contract(
		"_,_,_,!null,null -> fail;" +
			"null,_,_,_,_ -> fail;" +
			"_,null,_,_,_ -> fail;" +
			"!null,!null,_,null,null -> !null;" +
			"!null,!null,_,!null,!null -> !null;")
	public static Proxy create(@NonNull String id, @NonNull String host, int port, @Nullable String username, @Nullable String password) {
		Preconditions.checkArgument(!id.isEmpty(), "id may not be empty");
		Preconditions.checkArgument(!host.isEmpty(), "host may not be empty");
		return new Proxy(id, host, port, username, password);
	}

	@NonNull
	public static Proxy create(@NonNull String id, @NonNull String host, int port) {
		Preconditions.checkArgument(!id.isEmpty(), "id may not be empty");
		Preconditions.checkArgument(!host.isEmpty(), "host may not be empty");
		return new Proxy(id, host, port, null, null);
	}

	@SuppressWarnings("UnnecessaryParentheses")
	private Proxy(@NonNull String id, @NonNull String host, int port, @Nullable String username, @Nullable String password) {
		if ((username == null || username.trim().isEmpty()) && (password != null && !password.trim().isEmpty())) {
			throw new IllegalArgumentException("proxy " + id + ": the username has to be set if the password is set and vice versa!");
		}
		this.id = id;
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}

	public boolean isNoProxy() {
		return this == NO_PROXY;
	}

	@NonNull
	public String getId() {
		return id;
	}

	@NonNull
	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	@Nullable
	public String getUsername() {
		return username;
	}

	@Nullable
	public String getPassword() {
		return password;
	}

	public void setLastUsedMillis(@NonNull UUID uuid, long millis) {
		lastUsedMillis.put(uuid, millis);
	}

	public long getLastUsedMillis(@NonNull UUID uuid) {
		if (lastUsedMillis.containsKey(uuid))
			return lastUsedMillis.get(uuid);
		else {
			lastUsedMillis.put(uuid, 0L);
			return 0;
		}
	}
}
