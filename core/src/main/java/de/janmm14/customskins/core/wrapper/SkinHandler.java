package de.janmm14.customskins.core.wrapper;

import java.util.UUID;

import lombok.NonNull;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface SkinHandler {

	/**
	 * Updates a player's skin, silently ignoring if he'is not online.
	 *
	 * @param uuid the uuid of the player
	 */
	void updatePlayerSkin(@NonNull UUID uuid);
}
