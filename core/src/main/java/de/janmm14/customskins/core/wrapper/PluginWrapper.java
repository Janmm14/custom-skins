package de.janmm14.customskins.core.wrapper;

import java.io.File;
import java.util.Set;
import java.util.UUID;
import javax.annotation.Nullable;

import de.janmm14.customskins.core.data.Data;
import de.janmm14.customskins.core.wrapper.config.ConfigProvider;

import lombok.NonNull;

@SuppressWarnings("NullableProblems")
public interface PluginWrapper extends LoggerProvider, ConfigProvider, Wrapper {

	@NonNull
	Data getData();

	@NonNull
	SkinHandler getSkinHandler();

	@NonNull
	File getDataFolder();

	@NonNull
	SchedulerWrapper getSchedulerWrapper();

	@Nullable
	CommandSenderWrapper getPlayer(@NonNull UUID uuid);

	@Nullable
	CommandSenderWrapper getPlayer(@NonNull String name);

	@NonNull
	Set<CommandSenderWrapper> getOnlinePlayers();
}
