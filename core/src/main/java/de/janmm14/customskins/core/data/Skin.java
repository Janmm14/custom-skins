package de.janmm14.customskins.core.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
//TODO use ConfigurationSerializable so its easier for plugins later on when the api ... no, the api won't be ready in the next 10 years (I'm busy :( ), so do a pull request!
@RequiredArgsConstructor
@Getter
public class Skin { // TODO add skin source

	@NonNull
	private final String skinName;
	@NonNull
	private final String data;
	@NonNull
	private final String signature;

}
