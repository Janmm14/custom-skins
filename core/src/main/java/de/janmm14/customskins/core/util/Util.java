package de.janmm14.customskins.core.util;

import lombok.NonNull;

public final class Util {

	private Util() {
		throw new UnsupportedOperationException();
	}

	@NonNull
	public static String insertDashesToUuidString(@NonNull String dashFreeUuid) {
		return dashFreeUuid.substring(0, 8) + '-' + dashFreeUuid.substring(8, 12) + '-' + dashFreeUuid.substring(12, 16) + '-' + dashFreeUuid.substring(16, 20) + '-' + dashFreeUuid.substring(20, 32);
	}

	public static boolean isAlphanumeric(String str) {
		if (str == null) {
			return false;
		}
		int sz = str.length();
		for (int i = 0; i < sz; i++) {
			if (!Character.isLetterOrDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
