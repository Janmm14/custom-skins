package de.janmm14.customskins.core.data;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
//TODO use ConfigurationSerializable so its easier for plugins later on when the api ... no, the api won't be ready in the next 10 years (I'm busy :( ), so do a pull request!
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class Account {

	@NonNull
	private final String name;
	@NonNull
	private final UUID uuid;
	@NonNull
	private final String email;
	@NonNull
	private final String password;
	@NonNull
	private final AtomicBoolean used = new AtomicBoolean(false);

	@NonNull
	public String getName() {
		return name;
	}

	@NonNull
	public UUID getUuid() {
		return uuid;
	}

	@NonNull
	public String getEmail() {
		return email;
	}

	@NonNull
	public String getPassword() {
		return password;
	}

	@NonNull
	public AtomicBoolean getUsed() {
		return used;
	}
}
