The license for CustomSkins is based on the GNU GPL 3 license, which you can find in GNU_GPL3_LICENSE.txt

These additonal terms override the GNU GPL 3 license (these override GNU GPL v3):
  1) If any part of these rules clash with GNU GPL v3 license, these rules are used.
  1.1) If any of these rules is not applicable by law, a similar but legally working version of the rule has to be used, which is as much as possible similar to the original rule and fulfills its intends.
  1.2) If any of these rules is not applicable by law, the other rules stay intact and are not affected.
  2) Software and program is a synonym in the following additional terms and describe the source code and the binaries.
  2.1) The original author is Janmm14 / Jan Meyer.
  3) The original author of this program is allowed to use any code of this in any project he wants to with any license.
  4) ANY modification of this software which is not done by the original author must be made public; especially the source code.
  5) The original author is allowed to use your modifications as if it is a part of this project. So additonal term 2 applies to your modifications.
  6) Any project tht uses this resource in any kind must be made public (aource code & binaries). Except if it just uses the commands this resource provides.
  7) You may not simply copy the sources / the binary files somewhere and think this license does not apply anymore.
  8) You may not use this code as an example / outline of code that is not licensed under this license.
