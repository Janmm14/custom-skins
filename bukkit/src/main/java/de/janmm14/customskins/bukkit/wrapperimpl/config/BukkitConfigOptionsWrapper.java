package de.janmm14.customskins.bukkit.wrapperimpl.config;

import org.bukkit.configuration.file.FileConfigurationOptions;

import de.janmm14.customskins.core.wrapper.config.ConfigOptionsWrapper;

import lombok.NonNull;

public class BukkitConfigOptionsWrapper implements ConfigOptionsWrapper {

	@NonNull
	FileConfigurationOptions options;

	public BukkitConfigOptionsWrapper(@NonNull FileConfigurationOptions options) {
		this.options = options;
	}

	@NonNull
	@Override
	public ConfigOptionsWrapper copyDefaults(boolean value) {
		options.copyDefaults(value);
		return this;
	}

	@Override
	@NonNull
	public Object getWrapped() {
		return options;
	}
}
