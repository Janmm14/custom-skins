package de.janmm14.customskins.bukkit.wrapperimpl;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import de.janmm14.customskins.bukkit.CustomSkins;
import de.janmm14.customskins.core.commands.CommandHandler;

import lombok.NonNull;

public class BukkitCommandHandler extends CommandHandler implements TabExecutor {

	public BukkitCommandHandler(@NonNull CustomSkins plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
		return super.onCommand(new BukkitCommandSenderWrapper(sender), cmd.getName(), alias, args);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
		return super.onTabComplete(new BukkitCommandSenderWrapper(sender), cmd.getName(), alias, args);
	}

	@NonNull
	@Override
	protected CustomSkins getPlugin() {
		return (CustomSkins) super.getPlugin();
	}
}
