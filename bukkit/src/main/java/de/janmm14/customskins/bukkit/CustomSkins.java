package de.janmm14.customskins.bukkit;

import java.io.File;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.janmm14.customskins.bukkit.wrapperimpl.BukkitCommandHandler;
import de.janmm14.customskins.bukkit.wrapperimpl.BukkitCommandSenderWrapper;
import de.janmm14.customskins.bukkit.wrapperimpl.BukkitSchedulerWrapper;
import de.janmm14.customskins.bukkit.wrapperimpl.config.BukkitConfigWrapper;
import de.janmm14.customskins.core.data.Data;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;
import de.janmm14.customskins.core.wrapper.config.ConfigWrapper;

import com.comphenix.protocol.ProtocolLibrary;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NonNull;

@Getter
public class CustomSkins extends JavaPlugin implements PluginWrapper {

	@Getter(onMethod = @__({@Deprecated}))
	private static CustomSkins plugin;

	private Data data;
	private ProtocolLibSkinHandler skinHandler;
	private ConfigWrapper configWrapper;
	private BukkitSchedulerWrapper schedulerWrapper;

	@Override
	public void onEnable() {
		plugin = this;

		File cacheDir = new File(getDataFolder(), "cache");
		if (cacheDir.exists()) {
			if (!cacheDir.isDirectory()) {
				if (!cacheDir.delete()) {
					getLogger().severe("Could not delete file named like the cache directory. Missing permissions? (Path: " + cacheDir.getAbsolutePath() + " )");
				}
				if (!cacheDir.mkdirs()) {
					getLogger().severe("Could not create cache folder (#2). Missing permissions? (Path: " + cacheDir.getAbsolutePath() + " )");
				}
			}
		} else {
			if (!cacheDir.mkdirs()) {
				getLogger().severe("Could not create cache folder. Missing permissions? (Path: " + cacheDir.getAbsolutePath() + " )");
			}
		}
		//initialize wrappers
		configWrapper = new BukkitConfigWrapper(this, getConfig());
		schedulerWrapper = new BukkitSchedulerWrapper(this);
		data = new Data(this);

		//initialize skin handler / protocollib listener
		skinHandler = new ProtocolLibSkinHandler(this);
		ProtocolLibrary.getProtocolManager().getAsynchronousManager()
			.registerAsyncHandler(skinHandler)
			.start();

		//initialize command system
		PluginCommand cmd = getCommand("customskins");
		if (cmd == null) {
			getLogger().severe("###################################################");
			getLogger().severe("Command 'customskins' not found while initializing!");
			getLogger().severe("###################################################");
		} else {
			BukkitCommandHandler commandHandler = new BukkitCommandHandler(this);
			cmd.setExecutor(commandHandler);
			cmd.setTabCompleter(commandHandler);
		}

		/*try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			getLogger().warning("PluginMetrics (mcstats.org) error:");
			e.printStackTrace();
		}*/
	}

	@Override
	public void onDisable() {
		ProtocolLibrary.getProtocolManager().removePacketListeners(this);
		saveConfig();
		plugin = null;
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		getLogger().warning("Unknown command '" + cmd.getName() + "' was bound to CustomSkins.");
		cs.sendMessage("§cA command association problem occurred.");
		return true;
	}

	@Override
	@Nullable
	public CommandSenderWrapper getPlayer(@NonNull UUID uuid) {
		Player plr = getServer().getPlayer(uuid);
		if (plr == null) {
			return null;
		}
		return new BukkitCommandSenderWrapper(plr);
	}

	@Override
	@Nullable
	public CommandSenderWrapper getPlayer(@NonNull String name) {
		Player plr = getServer().getPlayerExact(name);
		if (plr == null) {
			return null;
		}
		return new BukkitCommandSenderWrapper(plr);
	}

	@NonNull
	@Override
	public Set<CommandSenderWrapper> getOnlinePlayers() {
		return getServer().getOnlinePlayers().stream()
			.map(BukkitCommandSenderWrapper::new)
			.collect(Collectors.toSet());
	}

	@Override
	@Nullable
	public Object getWrapped() {
		return this;
	}
}
