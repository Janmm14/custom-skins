package de.janmm14.customskins.bukkit.wrapperimpl;

import javax.annotation.Nullable;

import org.bukkit.scheduler.BukkitScheduler;

import de.janmm14.customskins.bukkit.CustomSkins;
import de.janmm14.customskins.core.wrapper.SchedulerWrapper;

import lombok.NonNull;

public class BukkitSchedulerWrapper implements SchedulerWrapper {
	private final CustomSkins plugin;
	private final BukkitScheduler scheduler;

	public BukkitSchedulerWrapper(CustomSkins plugin) {
		this.plugin = plugin;

		scheduler = plugin.getServer().getScheduler();
	}

	@Override
	public void asyncNow(@NonNull Runnable task) {
		scheduler.runTaskAsynchronously(plugin, task);
	}

	@Nullable
	@Override
	public Object getWrapped() {
		return scheduler;
	}
}
