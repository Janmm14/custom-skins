package de.janmm14.customskins.bukkit.wrapperimpl;

import java.util.UUID;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

public class BukkitCommandSenderWrapper implements CommandSenderWrapper {

	@NonNull
	private final CommandSender sender;

	public BukkitCommandSenderWrapper(@NonNull CommandSender sender) {
		this.sender = sender;
	}

	@NonNull
	@Override
	public String getName() {
		return sender.getName();
	}

	@NonNull
	@SuppressWarnings("OverlyStrongTypeCast")
	@Override
	public UUID getUuid() {
		if (isPlayer()) {
			return ((Player) sender).getUniqueId();
		} else {
			return CONSOLE_UUID;
		}
	}

	@Override
	public void sendMessage(@NonNull String legacyChat) {
		sender.sendMessage(legacyChat);
	}

	@Override
	public void sendMessage(@NonNull BaseComponent component) {
		if (isPlayer()) {
			((Player) sender).spigot().sendMessage(component);
		} else {
			sender.sendMessage(component.toLegacyText());
		}
	}

	@Override
	public void sendMessage(@NonNull BaseComponent... components) {
		if (isPlayer()) {
			((Player) sender).spigot().sendMessage(components);
		} else {
			sender.sendMessage(BaseComponent.toLegacyText(components));
		}
	}

	@Override
	public boolean hasPermission(@NonNull String permission) {
		return sender.hasPermission(permission);
	}

	@NonNull
	@Override
	public Type getType() {
		return sender instanceof Player ? Type.PLAYER : Type.CONSOLE;
	}

	@Override
	@Nullable
	public Object getWrapped() {
		return sender;
	}
}
