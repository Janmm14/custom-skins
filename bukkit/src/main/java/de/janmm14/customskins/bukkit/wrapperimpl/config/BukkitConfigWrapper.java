package de.janmm14.customskins.bukkit.wrapperimpl.config;

import org.bukkit.configuration.file.FileConfiguration;

import de.janmm14.customskins.bukkit.CustomSkins;
import de.janmm14.customskins.core.wrapper.config.ConfigOptionsWrapper;
import de.janmm14.customskins.core.wrapper.config.ConfigWrapper;

import lombok.NonNull;

public class BukkitConfigWrapper extends BukkitConfigurationSectionWrapper implements ConfigWrapper {

	@NonNull
	private final CustomSkins plugin;
	private final BukkitConfigOptionsWrapper configOptions;

	protected FileConfiguration getConfig() {
		return (FileConfiguration) cfg;
	}

	public BukkitConfigWrapper(@NonNull CustomSkins plugin, @NonNull FileConfiguration cfg) {
		super(cfg);
		this.plugin = plugin;
		configOptions = new BukkitConfigOptionsWrapper(getConfig().options());
	}

	@Override
	public void reloadConfig() {
		plugin.reloadConfig();
		//silent edit all available wrapper classes on config reload
		cfg = plugin.getConfig();
		configOptions.options = getConfig().options();
	}

	@Override
	public void saveConfig() {
		plugin.saveConfig();
	}

	@NonNull
	@Override
	public ConfigOptionsWrapper options() {
		return configOptions;
	}
}
